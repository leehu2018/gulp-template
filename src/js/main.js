new Vue({
    el: '#app',
    data: {
        list: [],
    },
    mounted() {
        this.go();
    },
    methods: {
        go() {
            fetch('/getList')
                .then((res) => res.json())
                .then((res) => {
                    if (res.code == 1) {
                        res.list.forEach((lis) => {
                            this.list.push(lis);
                        });
                    }
                });
        },
    },
});
