# gulp-template

#### 介绍
快速搭建传统型小型多页WEB项目的开发建构模板。

#### 功能

1. 支持sass代码的编译，压缩
1. 支持ES6,7代码转义为ES5，压缩，混淆
1. 支持图片压缩
1. 支持打包资源添加Hash值及动态修改html对应资源名和引用路径
1. 支持资源修改动态刷新浏览器


#### 安装教程

```
npm install
```

#### 使用说明

打开监听服务器
```
npm run server
```

编译,对代码不进行压缩混淆，打包后可给后台进行数据绑定
```
npm run buil:unpack
```

编译，对代码进行优化，压缩和混淆，适合前后端分离，打包后单独部署即可

```
npm run build:compress
```

